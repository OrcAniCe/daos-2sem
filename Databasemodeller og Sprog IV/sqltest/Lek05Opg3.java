package sqltest;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Scanner;

public class Lek05Opg3 {
	public static void main(String[] args) throws SQLException, IOException {
		SQLcon sql = new SQLcon();
		sql.open(Connection.TRANSACTION_SERIALIZABLE);

		PreparedStatement pstmt;
		try {
			pstmt = sql.pstmt(
					"INSERT INTO Medlem (medlemsId, navn, adresse, telefon, oprettelseDato) VALUES (51,?,?,?,'2005-04-12')");

			Scanner in = new Scanner(System.in);
			in.useDelimiter("\n");

			System.out.print("Indtast Navn: ");
			pstmt.setString(1, in.next());
			System.out.print("Indtast Adresse: ");
			pstmt.setString(2, in.next());
			System.out.print("Indtast Telefon: ");
			pstmt.setString(3, in.next());
			System.out.println("Rows updated" + pstmt.executeUpdate());
		} catch (Exception e) {
			e.printStackTrace();
		}

		sql.close();
	}
}