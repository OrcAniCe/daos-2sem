package sqltest;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SQLcon {
	private PreparedStatement pstmt = null;
	private Statement stmt = null;
	private Connection conn = this.connect();

	private Connection connect() {

		Connection conn = null;

		String dbURL = "jdbc:sqlserver://mssql4.gear.host";
		String user = "skole1";
		String pass = "Kw4XhVM~3C0_";
		// String datab = "skole1";

		try {
			conn = DriverManager.getConnection(dbURL, user, pass);
		} catch (SQLException e) {

		}
		return conn;
	}

	public void open(int type) {
		try {
			conn.setTransactionIsolation(type);
			conn.setAutoCommit(false);
			stmt = conn.createStatement();
		} catch (SQLException e) {

		}
	}

	public void close() {
		try {
			conn.commit();
			conn.setAutoCommit(true);
			stmt.close();
			conn.close();
		} catch (SQLException e) {

		}
	}

	public ResultSet query(String SQL) {
		try {
			return stmt.executeQuery(SQL);
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
		return null;
	}

	public PreparedStatement pstmt(String SQL) throws Exception {
		try {
			pstmt = conn.prepareStatement(SQL);
			// rs = pstmt.executeQuery(sql);
		} catch (SQLException ex) {
			System.err.println("Error:" + ex.getMessage());
		}
		return pstmt;
	}

	public void update(String SQL) {
		try {
			stmt.executeUpdate(SQL);
		} catch (SQLException e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

}
