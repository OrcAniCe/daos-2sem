package sqltest;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Lek05Opg1 {
	public static void main(String[] args) throws SQLException {

		SQLcon sql = new SQLcon();

		sql.open(Connection.TRANSACTION_SERIALIZABLE);
		ResultSet rs = sql.query("SELECT * FROM Medlem");
		while (rs.next()) {
			System.out.println(rs.getString("navn"));
		}
		sql.close();

	}
}