package sqltest;

import java.io.FileInputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Scanner;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class FilTest extends Application {
	public static void main(String[] args) {
		Application.launch(args);
	}

	@Override
	public void start(Stage stage) {
		stage.setTitle("SQL");
		GridPane pane = new GridPane();
		this.initContent(pane);

		Scene scene = new Scene(pane);
		stage.setScene(scene);
		stage.show();
	}

	// -------------------------------------------------------------------------

	private TextField txfDato;
	private ListView<String> lvwNames;
	private Button btnLav, btnHent;

	private final Controller controller = new Controller();

	private void initContent(GridPane pane) {
		pane.setGridLinesVisible(false);
		pane.setPadding(new Insets(20));
		pane.setHgap(10);
		pane.setVgap(10);

		Label lblName = new Label("Skriv og læs filer");
		pane.add(lblName, 0, 0);

		txfDato = new TextField();
		pane.add(txfDato, 1, 1, 2, 1);

		lvwNames = new ListView<>();
		pane.add(lvwNames, 0, 1, 1, 3);
		lvwNames.setPrefWidth(200);
		lvwNames.setPrefHeight(100);

		btnLav = new Button("Lav fil");
		pane.add(btnLav, 2, 2);
		btnLav.setOnAction(event -> this.controller.lavFilAction());

		btnHent = new Button("Hent fil");
		pane.add(btnHent, 2, 3);
		btnHent.setOnAction(event -> this.controller.hentAction());
	}

	private class Controller {
		private ArrayList<String> names;

		public Controller() {
		}

		private void lavFilAction() {
			try {
				String timeStamp = txfDato.getText();
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

				PrintWriter printWriter = new PrintWriter("/Users/andreaselkjaer/Documents/" + timeStamp + ".txt");

				SQLcon sql = new SQLcon();
				sql.open(Connection.TRANSACTION_SERIALIZABLE);

				ResultSet res = sql.query("exec MedlemmerHoldDato '2016-08-15'");
				System.out.println(timeStamp);
				while (res.next()) {
					printWriter.println(res.getString(1) + "    " + res.getString(2) + "\t");
					printWriter.flush();
				}
				printWriter.close();
				if (res != null) {
					res.close();
				}
			} catch (Exception e) {
				System.out.println("fejl:  " + e.getMessage());
			}
		}

		private void hentAction() {
			names = new ArrayList<>();

			try {
				String filenavn = txfDato.getText();
				FileInputStream filein = new FileInputStream("/Users/andreaselkjaer/Documents/" + filenavn + ".txt");

				Scanner scan = new Scanner(filein);
				while (scan.hasNext()) {
					String s = scan.nextLine();
					names.add(s);
				}
				lvwNames.getItems().setAll(names);
			} catch (Exception e) {
				System.out.println("fejl:  " + e.getMessage());
			}
		}
	}
}