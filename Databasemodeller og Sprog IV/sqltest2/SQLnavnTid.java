package sqltest2;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class SQLnavnTid {
	public static void main(String[] args) throws SQLException {

		SQLcon sql = new SQLcon();

		sql.open(Connection.TRANSACTION_SERIALIZABLE);
		ResultSet rs = sql.query(
				"SELECT navn, convert(char(5), startTid, 108) AS startTid FROM Hold WHERE dag = 'Tirsdag' AND uge = '33'");
		while (rs.next()) {
			System.out.println(rs.getString("navn") + "  " + rs.getString("startTid"));
		}
		sql.close();

	}
}