package sqltest2;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TestSQL {
	public static void main(String[] args) throws SQLException {

		SQLcon sql = new SQLcon();

		sql.open(Connection.TRANSACTION_SERIALIZABLE);
		ResultSet rs = sql.query("SELECT * FROM Bank");
		while (rs.next()) {
			System.out.println("Navn: " + rs.getString("navn"));
			System.out.println("Adresse: " + rs.getString("adresse"));
			System.out.println("PostNr: " + rs.getString("postNr"));
			System.out.println();
		}
		sql.close();

	}
}