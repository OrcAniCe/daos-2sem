package opgave2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class MainApp {

	public static void main(String[] args) {
		try {
			Connection minConnection;
			String dbURL = "jdbc:sqlserver://mssql4.gear.host";
			String user = "skole2";
			String pass = "Lr3CUV7648~~";
			minConnection = DriverManager.getConnection(dbURL, user, pass);

			Statement stmt = minConnection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE); // opretter
																															// statement
																															// object

			ResultSet res = stmt.executeQuery("select * from Afdeling");
			while (res.next()) {
				System.out.println("RegNr: " + res.getString("regNr") + " Navn: " + res.getString("navn") + " Adresse: "
						+ res.getString("adresse") + " Telefon: " + res.getString("tlfNr"));
			}
			if (res != null) {
				res.close();
			}
			if (stmt != null) {
				stmt.close();
			}
			if (minConnection != null) {
				minConnection.close();
			}
		} catch (Exception e) {
			System.out.println("fejl:  " + e.getMessage());
		}
	}
}
