package sqlEksempler;

import java.sql.*;

public class SQLSelectFirma {

	public static void main(String[] args) {
		try {
			Connection minConnection;
			minConnection = DriverManager
					.getConnection("jdbc:sqlserver://EAA-SH-STHA;databaseName=JDBCDatabase;user=sa;password=HelloWorld;");

			Statement stmt = minConnection.createStatement();

			ResultSet res = stmt.executeQuery("select * from Firma_tbl");
			while (res.next()) {
				System.out
						.println(res.getString(1) + "    " + res.getString(2));
			}

			if (res != null)
				res.close();
			if (stmt != null)
				stmt.close();
			if (minConnection != null)
				minConnection.close();
		} catch (Exception e) {
			System.out.println("fejl:  " + e.getMessage());
		}
	}
}
