package sqlEksempler;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class SQLOpretByPre {

	public static void main(String[] args) {
		try {
			System.out.println("Vi vil nu oprette en ny by : ");
			BufferedReader inLine = new BufferedReader(new InputStreamReader(
					System.in));
			System.out.print("Indtast postnummer: ");
			String postnummer = inLine.readLine();
			System.out.println("Indtast bynavn: ");
			String navn = inLine.readLine();
			System.out.println("Indtast indbyggertal: ");
			int indbygger = Integer.parseInt(inLine.readLine());
			Connection minConnection;
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			minConnection = DriverManager
					.getConnection("jdbc:sqlserver://EAA-SH-STHA\\SQLEXPRESS;databaseName=JDBCDatabase;user=sa;password=HelloWorld;");
			String sql = "insert into City_tbl values (?,?,?)";// preparedStatement
			PreparedStatement prestmt = minConnection.prepareStatement(sql);
			prestmt.clearParameters();
			prestmt.setString(1, postnummer);
			prestmt.setString(2, navn);
			prestmt.setInt(3, indbygger);
			prestmt.execute();
			System.out.println("By er nu registreret");
			if (prestmt != null)
				prestmt.close();
			if (minConnection != null)
				minConnection.close();

		} catch (SQLException e) {
			switch (e.getErrorCode()) {
			case 547: {
				if (e.getMessage().indexOf("") != -1)
					System.out.println("");
				break;
			}
			case 2627: {
				System.out.println("den pågældende by er allerede oprettet");
				break;
			}
			default:
				System.out.println("fejlSQL:  " + e.getMessage());
			}
			;
		} catch (Exception e) {
			System.out.println("fejl:  " + e.getMessage());
		}
	}

}
