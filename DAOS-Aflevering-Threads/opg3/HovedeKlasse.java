package opg3;

public class HovedeKlasse {

	public static void main(String[] args) {
		FaellesKlasse faellesKlasse = new FaellesKlasse();
		TraadKlasse traad1 = new TraadKlasse("Traad1", faellesKlasse);
		TraadKlasse traad2 = new TraadKlasse("Traad2", faellesKlasse);
		traad1.start();
		traad2.start();
	}
}
