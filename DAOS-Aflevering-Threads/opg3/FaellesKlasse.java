package opg3;

public class FaellesKlasse {

	private int global = 0;

	public synchronized void tagerRandomTid(int max) {
		int counter = 0;
		int random = (int) (Math.random() * max) + 1;
		for (int i = 0; i < random; i++) {
			for (int j = 0; j < random; j++) {
				counter = i + j;
			}
		}
	}

	public int getGlobal() {
		return global;
	}

	public synchronized void kritiskSection() {
		int temp = global;
		this.tagerRandomTid(100);
		global = ++temp;
	}

}
