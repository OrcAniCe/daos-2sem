package opg1;

public class TraadKlasse extends Thread {
	private String traadNavn;
	private FaellesKlasse faellesKlasse;

	public TraadKlasse(String traadNavn, FaellesKlasse faellesKlasse) {
		this.traadNavn = traadNavn;
		this.faellesKlasse = faellesKlasse;
	}

	@Override
	public void run() {
		for (int j = 0; j < 100; j++) {
			faellesKlasse.kritiskSection();
			faellesKlasse.tagerRandomTid(100);
		}
		System.out.println(traadNavn + " " + faellesKlasse.getGlobal());
	}
}
