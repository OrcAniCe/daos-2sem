package opg2;

public class FaellesKlasse {

	private int global = 0;
	private volatile boolean[] flag = new boolean[2];
	private volatile int turn;

	public FaellesKlasse() {
		flag[0] = false;
		flag[1] = false;
	}

	public void tagerRandomTid(int max) {
		int counter = 0;
		int random = (int) (Math.random() * max) + 1;
		for (int i = 0; i < random; i++) {
			for (int j = 0; j < random; j++) {
				counter = i + j;
			}
		}
	}

	public int getGlobal() {
		return global;
	}

	public void kritiskSection() {
		int temp = global;
		tagerRandomTid(100);
		global = ++temp;
	}

	public boolean getFlag(int n) {
		return flag[n];
	}

	public int getTurn() {
		return turn;
	}

	public void setFlag(int n, boolean t) {
		flag[n] = t;
	}

	public void setTurn(int n) {
		turn = n;
	}

}
