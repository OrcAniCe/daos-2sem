package opg2;

public class HovedeKlasse {

	public static void main(String[] args) {
		FaellesKlasse faellesKlasse = new FaellesKlasse();
		TraadKlasse traad1 = new TraadKlasse("Traad1", faellesKlasse, 0);
		TraadKlasse traad2 = new TraadKlasse("Traad2", faellesKlasse, 1);
		traad1.start();
		traad2.start();
	}
}
