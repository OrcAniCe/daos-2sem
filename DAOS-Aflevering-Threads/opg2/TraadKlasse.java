package opg2;

public class TraadKlasse extends Thread {
	private String traadNavn;
	private FaellesKlasse faellesKlasse;
	private int id;

	public TraadKlasse(String traadNavn, FaellesKlasse faellesKlasse, int id) {
		this.traadNavn = traadNavn;
		this.faellesKlasse = faellesKlasse;
		this.id = id;
	}

	@Override
	public void run() {
		int sid = (id + 1) % 2;
		for (int j = 0; j < 100; j++) {
			faellesKlasse.setFlag(id, true);
			faellesKlasse.setTurn(sid);
			while (faellesKlasse.getFlag(sid) && faellesKlasse.getTurn() == sid) {
			}
			faellesKlasse.kritiskSection();
			faellesKlasse.setFlag(id, false);
			faellesKlasse.tagerRandomTid(100);
		}
		System.out.println(traadNavn + " " + faellesKlasse.getGlobal());
	}
}
